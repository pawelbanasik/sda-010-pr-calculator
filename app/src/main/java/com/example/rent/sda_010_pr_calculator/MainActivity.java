package com.example.rent.sda_010_pr_calculator;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.button_one)
    protected Button buttonOne;

    @BindView(R.id.button_two)
    protected Button buttonTwo;

    @BindView(R.id.button_plus)
    protected Button buttonPlus;

    @BindView(R.id.edit_text_one)
    protected EditText editTextOne;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

    }

    @OnClick ({R.id.button_one, R.id.button_two, R.id.button_plus})
    public void calculationMethod(View v){

        Button b = (Button) v;

        editTextOne.setText(editTextOne.getText().toString() + b.getText());

    }
}
